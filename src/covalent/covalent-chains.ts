import {CovalentChainData, CovalentResponseFormat} from './covalent-types.js';
import * as fs from 'fs';
import * as covalent_api from './covalent-api.js';
import {getChainsFromFile} from '../common/available-chains.js';
import dotenv from 'dotenv';

dotenv.config();
const covalentChainsFilePath: string = process.env
  .COVALENT_CHAINS_JSON as string;

const axiosThrowError = false;

/* Retrieve chains from api */
async function getChainsFromApi(): Promise<CovalentChainData[]> {
  const response = await covalent_api.call(`chains/`, axiosThrowError);
  if (response !== null) {
    const data = (response as unknown as CovalentResponseFormat).data;
    const chains = data as CovalentChainData[];
    return chains;
  } else {
    return [] as CovalentChainData[]; // return empty array
  }
}

/* Retrieve chains from api */
async function updateChainsFile(): Promise<void> {
  const chainData = await getChainsFromApi();
  const data = JSON.stringify(chainData, null, 2);
  fs.writeFileSync(covalentChainsFilePath, data);
  console.log(
    `Covalent chains file has been updated: ${covalentChainsFilePath}`
  );
  await missingChains();
}

/* Retrieve chains from api */
async function missingChains(): Promise<void> {
  const covalentChainData = await getChainsFromApi();
  const generalChainData = getChainsFromFile();

  covalentChainData
    .filter((x) => !x.is_testnet)
    .forEach((x) => {
      const result = generalChainData.filter(
        (y) => y.covalent_chain_id == x.chain_id
      );
      if (result.length == 0) {
        console.log('New Covalent Chain Found:');
        console.log(JSON.stringify(x, null, 2));
      }
    });
}

export {updateChainsFile};
