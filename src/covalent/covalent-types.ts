import Big, {BigSource} from 'big.js';

interface CovalentResponseFormat {
  data: unknown;
  error: boolean;
  error_message: string | null;
  error_code: string | null;
}

interface CovalentPagination {
  has_more: boolean; // true if we can paginate to get more data.
  page_number: number; // The specific page being returned.
  page_size: number; // The number of results per page.
  total_count: number; // Total number of entries.
}

interface CovalentChainData {
  name: string;
  chain_id: string;
  is_testnet: boolean;
  db_schema_name: string;
  label: string;
  logo_url: string;
  symbol?: string;
}

interface CovalentTransactionResponse {
  address: string; // The requested wallet address.
  updated_at: Date; // The updated time.
  next_update_at: Date; // The next updated time.
  quote_currency: string; // The requested fiat currency.
  chain_id: number; // The requested chain ID.
  items: CovalentBlockTransaction[]; // The transactions.
  pagination: CovalentPagination;
}

interface CovalentBlockTransaction {
  block_signed_at: string; // The signed time of the block.
  block_height: Big; // The height of the block.
  tx_hash: string; // The transaction hash.
  tx_offset: number; // The transaction offset.
  successful: boolean; // The transaction status.
  from_address: string; // The address where the transaction is from.
  from_address_label: string | null; // The label of from address.
  to_address: string; // The address where the transaction is to.
  to_address_label: string | null; // The label of to address.
  value: Big; // The value attached to this tx.
  value_quote: number; // The value attached in quote-currency to this tx.
  gas_offered: number; // The gas offered for this tx.
  gas_spent: Big; // The gas spent for this tx.
  gas_price: Big; //The gas price at the time of this tx.
  gas_quote: Big; // The gas spent in quote-currency denomination.
  gas_quote_rate: Big; // Historical ETH price at the time of tx.
  log_events: CovalentLogEvent[]; // The log events
}

interface CovalentLogEvent {
  block_signed_at: string | Date; // The signed time of the block.
  block_height: number; // The height of the block.
  tx_offset: number; // The transaction offset.
  log_offset: number; //The log offset.
  tx_hash: string; // The transaction hash.
  _raw_log_topics_bytes: unknown;
  raw_log_topics: unknown;
  sender_contract_decimals: number; // Smart contract decimals.
  sender_name: string; // Smart contract name.
  sender_contract_ticker_symbol: string; // Smart contract ticker symbol.
  sender_address: string; // The address of the sender.
  sender_address_label: string | null; // The label of the sender address.
  sender_logo_url: string; // Smart contract URL.
  raw_log_data: string; //The log events in raw.
  decoded: CovalentDecodedItem;
}

interface CovalentDecodedItem {
  name: string; // The name of the decoded item.
  signature: string; // The signature of the decoded item.
  params: CovalentDecodedParamItem[]; // The parameters of the decoded item.
}

interface CovalentDecodedParamItem {
  name: string; // The name of the parameter.
  type: string; // The type of the parameter.
  indexed: boolean; // The index of the parameter.
  decoded: boolean; // The decoded value of the parameter.
  value: unknown; // The value of the parameter.
}

interface CovalentTokenBalanceItem {
  address: string; // The requested wallet address.
  items: CovalentWalletBalanceItem[];
  next_update_at: Date; // The next updated time.
  quote_currency: string; // The requested fiat currency.
  updated_at: Date; // The updated time.
}

interface CovalentWalletBalanceItem {
  balance: string; // The asset balance. Use contract_decimals to scale this balance for display purposes.
  balance_24h: string; // The asset balance 24 hours ago.
  contract_address: string; // Smart contract address.
  contract_decimals: number; // Smart contract decimals.
  contract_name: string; // Smart contract name.
  contract_ticker_symbol: string; // Smart contract ticker symbol.
  last_transferred_at: Date; // Last transferred date for a wallet
  logo_url: string; // Smart contract URL.
  quote: number; // The current balance converted to fiat in quote-currency.
  quote_24h: number | null; // The current balance converted to fiat in quote-currency as of 24 hours ago.
  quote_rate: number; // The current spot exchange rate in quote-currency.
  quote_rate_24h: number | null; // The spot exchange rate in quote-currency as of 24 hours ago.
  supports_erc: unknown; // The standard interface(s) supported for this token, eg: ERC-20.
  type: string; // One of cryptocurrency, stablecoin, nft or dust.
  nft_data: unknown; // Array of NFTs that are held under this contract.
}

export type {
  CovalentBlockTransaction,
  CovalentChainData,
  CovalentDecodedItem,
  CovalentDecodedParamItem,
  CovalentLogEvent,
  CovalentResponseFormat,
  CovalentTokenBalanceItem,
  CovalentTransactionResponse,
  CovalentWalletBalanceItem,
};
