import {
  CovalentTokenBalanceItem,
  CovalentWalletBalanceItem,
} from './covalent-types.js';
import * as covalent_api from './covalent-api.js';
import Big from 'big.js';
import dotenv from 'dotenv';

dotenv.config();
const quote_currency = process.env.COVALENT_QUOTE_CURRENCY as string;

/*
Get token balances for address
Given chain_id and wallet address, returns current token balances along 
with their spot prices. This endpoint supports a variety of token standards
like ERC20, ERC721 and ERC1155. As a special case, network native tokens
like ETH on Ethereum are also returned even though it's not a token contract.


Additional query options:
quote-currency (type: string, default: USD)
nft (default: false): Set to true to return ERC721 and ERC1155 assets (default: false)
no-nft-fetch (default: false) Set to true to skip fetching NFT metadata, which can result in faster responses. Only applies when nft=true.
*/

async function balances(
  chain_id: string,
  address: string
): Promise<CovalentTokenBalanceItem | null> {
  const url_slug = `${chain_id}/address/${address}/balances_v2/`;
  const params = {
    'quote-currency': quote_currency,
  };
  const response = await covalent_api.call(url_slug, params);
  return response !== null ? (response.data as CovalentTokenBalanceItem) : null;
}

function token_balance(
  tokens: CovalentWalletBalanceItem[],
  token_symbol: string
): Big {
  const token = tokens.find((x) => x.contract_ticker_symbol === token_symbol);
  if (token !== undefined) {
    return Big(token.balance);
  } else {
    throw `Could not find token_symbol: ${token_symbol}`;
  }
}

export {balances, token_balance};
