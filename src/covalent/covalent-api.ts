import axios, {
  AxiosError,
  AxiosPromise,
  AxiosResponse,
  AxiosResponseHeaders,
} from 'axios';
import {axiosErrorHandler} from '../common/axios-error-handler';
import dotenv from 'dotenv';
dotenv.config();

const url = process.env.COVALENT_API_URL as string;
const key = process.env.COVALENT_API_KEY as string;
const headers = 'content-type: application/json';

async function call(
  url_slug: string,
  params = {},
  throwError = true,
  transform?: (data: unknown, headers?: AxiosResponseHeaders) => unknown
): Promise<AxiosPromise<unknown> | null> {
  try {
    const result = await axios.get(`${url}/${url_slug}?key=${key}`, {
      headers: {headers},
      params: params,
      transformResponse: transform,
    });
    return result.data as AxiosResponse<unknown>;
  } catch (error: AxiosError | unknown) {
    axiosErrorHandler(error, throwError);
    return null;
  }
}

export {call};
