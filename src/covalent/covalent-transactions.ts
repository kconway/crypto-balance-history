import {
  CovalentTransactionResponse,
  CovalentResponseFormat,
  CovalentBlockTransaction,
} from './covalent-types.js';
import Big, {BigSource} from 'big.js';
import {AxiosResponseTransformer} from 'axios';
import * as covalent_api from './covalent-api.js';

const axiosThrowError = false;

/*
Get transactions for address
Given chain_id and wallet address, return all transactions along 
with their decoded log events. This endpoint does a deep-crawl 
of the blockchain to retrieve all kinds of transactions that references 
the address including indexed topics within the event logs.

Additional query options:
block-signed-at-asc (default: false) Sort the transactions in chronological ascending order.
By default, it's set to false and returns transactions in chronological descending order.
no-logs (default: false) Setting this to true will omit decoded event logs, 
resulting in lighter and faster responses. By default it's set to false.
page-number (type: integer) The specific page to be returned.
page-size (type: integer) The number of results per page.
*/

const transform: AxiosResponseTransformer = (data: string) => {
  const obj = JSON.parse(data);
  obj.data.items.forEach(
    (element: {
      value: BigSource;
      gas_price: BigSource;
      gas_spent: BigSource;
    }) => {
      element.value = Big(element.value);
      element.gas_price = Big(element.gas_price);
      element.gas_spent = Big(element.gas_spent);
    }
  );
  return obj;
};

async function transactions(
  chain_id: string,
  address: string,
  from: Date,
  to: Date,
  logs = false
): Promise<CovalentBlockTransaction[]> {
  const noLogsOption = !logs; // see no-logs option above
  const pageSizeOption = 100; // see page-size option above
  let pageNum = 0;

  let morePages = true;
  const data: CovalentBlockTransaction[] = [];
  while (morePages) {
    const url_slug = `${chain_id}/address/${address}/transactions_v2/`;
    const params = {
      'no-logs': noLogsOption,
      'page-number': pageNum,
      'page-size': pageSizeOption,
    };
    const response = (await covalent_api.call(
      url_slug,
      params,
      axiosThrowError,
      transform
    )) as unknown as CovalentResponseFormat;

    if (response !== null) {
      const partialData = (response.data as CovalentResponseFormat)
        .data as CovalentTransactionResponse;
      data.push(...partialData.items);

      morePages = partialData.pagination.has_more;
      pageNum = partialData.pagination.page_number + 1;

      const oldestTransDate = new Date(
        partialData.items[partialData.items.length - 1].block_signed_at
      );
      if (oldestTransDate <= from) {
        morePages = false; // exit loop since we have gone back far enough
      }
    }
  }
  return data.filter((x) => {
    const transDate = new Date(x.block_signed_at);
    return transDate >= from && transDate <= to;
  });
}

/* Given chain_id and tx_hash, return the transaction data 
with their decoded event logs (optional). */
async function transaction(
  chain_id: string,
  tx_hash: string,
  logs = false
): Promise<CovalentBlockTransaction | null> {
  const noLogsOption = !logs; // see no-logs option above
  const url_slug = `${chain_id}/transaction_v2/${tx_hash}/`;
  const params = {
    'no-logs': noLogsOption,
  };
  const response = (await covalent_api.call(
    url_slug,
    params,
    axiosThrowError,
    transform
  )) as unknown as CovalentResponseFormat;
  return response !== null
    ? ((response.data as CovalentTransactionResponse).items.at(
        0
      ) as CovalentBlockTransaction)
    : null;
}

export {transaction, transactions};
