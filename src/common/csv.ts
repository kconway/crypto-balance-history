import {Parser} from 'json2csv';
import * as fs from 'fs/promises';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function csvFromJson(filePath: string, data: any): Promise<void> {
  const json2csv = new Parser();
  const csv = json2csv.parse(data);
  await fs.writeFile(filePath, csv);
}

export {csvFromJson};
