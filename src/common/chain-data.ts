import axios from 'axios';
import {axiosErrorHandler} from './axios-error-handler';
import * as fs from 'fs/promises';

const url =
  'https://github.com/trustwallet/assets/archive/refs/heads/master.zip';

async function trustWalletData(): Promise<void> {
  try {
    const response = await axios(url, {
      responseType: 'arraybuffer',
    });
    await fs.writeFile('./temp', response.data);
  } catch (error) {
    axiosErrorHandler(error, true);
  }
}
