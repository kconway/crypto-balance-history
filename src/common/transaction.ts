import {transactions as covalent_transactions} from '../covalent/covalent-transactions.js';
import {DataSource} from './data-sources';
import {available_chains} from './available-chains';
import Big from 'big.js';

interface Transaction {
  datetime: Date;
  received_qty: Big;
  received_sym: string;
  sent_qty: Big;
  sent_sym: string;
  fee_qty: Big;
  fee_sym: string;
  balance?: Big;
  transaction_hash?: string;
}

function calculateBalances(
  transactions: Transaction[],
  initialBalance: number
): Transaction[] {
  transactions.sort((a, b) => a.datetime.valueOf() - b.datetime.valueOf());
  let currentBalance = Big(initialBalance);
  transactions.forEach((x) => {
    currentBalance = currentBalance.add(x.received_qty.sub(x.sent_qty));
    if (x.sent_sym == x.fee_sym) {
      // only add fees if it was sent by that address
      currentBalance = currentBalance.sub(x.fee_qty);
    }
    x.balance = currentBalance;
  });
  return transactions;
}

async function getTransactions(
  base_currency_symbol: string,
  address: string,
  from: Date,
  to: Date
): Promise<Transaction[]> {
  // lookup symbol to find best datasource (for now only covalent)
  const chainInfo = available_chains();
  const chain = chainInfo.find(
    (x) =>
      x.base_currency_symbol.toUpperCase() ===
      base_currency_symbol.toUpperCase()
  );
  if (chain) {
    switch (+chain.source) {
      case DataSource.bitquery: {
        return [];
      }
      case DataSource.covalent:
      default: {
        const rawTransactions = await covalent_transactions(
          chain.covalent_chain_id.toString(),
          address,
          from,
          to
        );

        const mappedTransactions: Transaction[] = [];
        rawTransactions.forEach((x) => {
          const isSender =
            address.toUpperCase() === x.from_address.toUpperCase();
          const isReceiver =
            address.toUpperCase() === x.to_address.toUpperCase();
          const isContract = !isSender && !isReceiver;

          if (isContract) {
            //const contractTransactions = parseContractTransactions(x);
            //mappedTransactions.push(...contractTransactions);
          } else {
            const trans = {
              datetime: new Date(x.block_signed_at),
              received_qty: isSender ? Big(0.0) : x.value.mul(Big(10e-19)),
              received_sym: isSender ? '' : base_currency_symbol,
              sent_qty: isSender ? x.value.mul(Big(10e-19)) : Big(0.0),
              sent_sym: isSender ? base_currency_symbol : '',
              fee_qty:
                isSender && x.successful
                  ? x.gas_spent.mul(x.gas_price).mul(Big(10e-19))
                  : Big(0.0),
              fee_sym: base_currency_symbol,
              transaction_hash: x.tx_hash,
            };
            mappedTransactions.push(trans);
          }
        });
        return mappedTransactions;
      }
    }
  } else {
    throw new Error(`Symbol ${base_currency_symbol} is not in database`);
  }
}

export type {Transaction};
export {getTransactions, calculateBalances};
