import axios, {AxiosError} from 'axios';

function axiosErrorHandler(error: AxiosError | unknown, throwError: boolean) {
  if (axios.isAxiosError(error)) {
    // Axios error
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data.err);
      console.log(error.response.status);
      console.log(error.response.headers);
      if (throwError) {
        throw new Error(
          `HTTP Response - ${error.response.status} ${error.response.statusText}`
        );
      }
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
      if (throwError) {
        throw new Error(error.request);
      }
    } else if (error.message) {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
      if (throwError) {
        throw new Error(error.message);
      }
    }
    console.log(error.config);
  } else {
    if (throwError) {
      throw error;
    }
  }
}

export {axiosErrorHandler};
