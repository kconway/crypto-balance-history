import {ChainInfo} from './chain-info.js';
import * as fs from 'fs';
import {DataSource} from './data-sources.js';
import dotenv from 'dotenv';

dotenv.config();
const chainsFilePath: string = process.env.CHAINS_JSON as string;

function available_chains(): ChainInfo[] {
  const covalent_chains = getChainsFromFile();
  const all_chains = covalent_chains;
  return all_chains;
}

/* Retrieve chains from file */
function getChainsFromFile(): ChainInfo[] {
  if (fs.existsSync(chainsFilePath)) {
    const data = fs.readFileSync(chainsFilePath, 'utf-8');
    const jsonData = JSON.parse(data).items as ChainInfo[];
    const availChains = jsonData.map<ChainInfo>((x) => ({
      source: DataSource.covalent,
      name: x.name,
      base_currency_symbol: x.base_currency_symbol,
      covalent_chain_id: x.covalent_chain_id,
    }));
    return availChains;
  }
  return [] as ChainInfo[]; // return empty array
}

export type {ChainInfo};
export {available_chains, getChainsFromFile};
