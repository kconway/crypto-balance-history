import {DataSource} from './data-sources';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
enum Chains {}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
enum Symbols {}

interface Token {
  balance: string;
  contract_address: string;
  contract_decimals: number;
  contract_name: string;
  contract_ticker_symbol: string;
  last_transferred_at: Date;
  supports_erc: unknown;
  type: string;
}

interface ChainInfo {
  source: DataSource;
  name: string;
  base_currency_symbol: string;
  covalent_chain_id: string;
}

export type {ChainInfo, Token};
