import {ethers} from 'ethers';

async function resolveAddress(address: string): Promise<string> {
  if (address.toUpperCase().endsWith('.ETH')) {
    // resolve .eth name to address
    const provider = new ethers.providers.CloudflareProvider();
    const response = await provider.resolveName(address);
    if (!response) {
      throw 'Invalid ENS address.';
    } else {
      return response;
    }
  } else {
    return address;
  }
}

export {resolveAddress};
