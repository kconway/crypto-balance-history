import {balances} from './covalent/covalent-accounts.js';
import {available_chains} from './common/available-chains.js';

const address = '0x5e15502047b730B8118367303A73068dA55C221F';
const symbol = 'ETH'; // symbol
const chain_id = '56'; // '1' = ETH, '56' = BSC
const tx_hash =
  '0x87fd4e784d77232b3420104359175be94e23ab743ba1571e2008cc39e45fc180';
const tStartDate = new Date();
tStartDate.setMonth(tStartDate.getMonth() - 1);
const tEndDate = new Date();

const main = async () => {
  const result = await balances(chain_id, address);
  console.log(JSON.stringify(result, null, 2));
};
// eslint-disable-next-line @typescript-eslint/no-floating-promises
main();
