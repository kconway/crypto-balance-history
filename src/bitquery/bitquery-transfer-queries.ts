import { gql } from 'graphql-request';

export const queryBySender = gql`
  query ($network: EthereumNetwork!, $address: String!) {
    ethereum(network: $network) {
      transfers(receiver: { is: $address }) {
        currency {
          symbol
        }
        receiver {
          address
        }
        success
        amount
        block {
          timestamp {
            iso8601
          }
        }
        transaction {
          hash
          txFrom {
            address
          }
        }
        sender {
          address
        }
        gasValue
      }
    }
  }
`;

export const queryByRecipient = gql`
  query ($network: EthereumNetwork!, $address: String!) {
    ethereum(network: $network) {
      transfers(sender: { is: $address }) {
        currency {
          symbol
        }
        receiver {
          address
        }
        success
        amount
        block {
          timestamp {
            iso8601
          }
        }
        transaction {
          hash
          txFrom {
            address
          }
        }
        sender {
          address
        }
        gasValue
      }
    }
  }
`;
