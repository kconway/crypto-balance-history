import {queryBySender, queryByRecipient} from './bitquery-transfer-queries.js';
import {Transaction} from '../common/transaction.js';
import {Transfer} from './transfer.js';

// const bitquery_examples = async () => {
//   const network = 'bsc';
//   const address = '0x1A040d3459F924AB2cE73b90BCF060CE6eFF1361';
//   const data1 = await bitQueryClient(network, address, queryBySender);

//   const data2 = await bitQueryClient(network, address, queryByRecipient);
//   if (data1 != undefined && data2 != undefined) {
//     const data3 = [].concat(data1.ethereum.transfers, data2.ethereum.transfers);

//     console.log('Data 1 Count: ' + data1.ethereum.transfers.length);
//     console.log('Data 2 Count: ' + data2.ethereum.transfers.length);
//     console.log('Data 3 Count: ' + data3.length);

//     data3.forEach((transfer: Transfer) => {
//       let transaction: Transaction = S;

//       // only record value transfer if it is a success
//       // fee is recorded even when it is not successful
//       if (transfer.success) {
//         transaction.datetime = new Date(transfer.block.timestamp.iso8601);

//         if (transfer.sender.address.toUpperCase() == address.toUpperCase()) {
//           transaction.sent_qty = transfer.amount;
//           transaction.sent_sym = transfer.currency.symbol;
//         }

//         // Note: don't use elseif in case transaction was from/to self
//         if (transfer.receiver.address.toUpperCase() == address.toUpperCase()) {
//           transaction.received_qty = transfer.amount;
//           transaction.received_sym = transfer.currency.symbol;
//         }
//       }

//       transaction.fee_qty = transfer.gasValue;
//       transaction.fee_sym = 'BNB';

//       console.log(JSON.stringify(transaction, undefined, 2));
//     });
//   }
// };

// bitquery_examples();
