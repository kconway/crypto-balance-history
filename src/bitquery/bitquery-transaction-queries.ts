import { gql } from 'graphql-request';

export const queryBySender = gql`
  query ($network: EthereumNetwork!, $address: String!) {
    ethereum(network: $network) {
      transactions(txSender: { is: $address }) {
        currency {
          symbol
        }
        block {
          timestamp {
            iso8601
          }
        }
        sender {
          address
        }
        to {
          address
        }
        gasValue
        gasCurrency {
          symbol
        }
        success
        amount
      }
    }
  }
`;

export const queryByRecipient = gql`
  query ($network: EthereumNetwork!, $address: String!) {
    ethereum(network: $network) {
      transactions(txTo: { is: $address }) {
        currency {
          symbol
        }
        block {
          timestamp {
            iso8601
          }
        }
        sender {
          address
        }
        gasValue
        gasCurrency {
          symbol
        }
        success
        amount
      }
    }
  }
`;
