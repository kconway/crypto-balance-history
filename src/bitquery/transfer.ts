export interface Transfer {
  success: boolean;
  datetime: Date;
  amount: number;
  currency: {
    symbol: string;
  };
  block: {
    timestamp: {
      iso8601: string;
    };
  };
  sender: {
    address: string;
  };
  receiver: {
    address: string;
  };
  gasValue: number;
}
