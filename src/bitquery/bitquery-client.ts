import dotenv from 'dotenv';
import {GraphQLClient} from 'graphql-request';

// dotenv.config();

// async function bitQueryCall(
//   chain: string,
//   address: string,
//   gqlQueryFunction: string
// ): Promise<unknown> {
//   const url = process.env.BITQUERY_API_URL as string;
//   const graphQLClient = new GraphQLClient(url, {
//     headers: [
//       ['Content-Type', 'application/json'],
//       ['X-API-KEY', process.env.BITQUERY_API_KEY as string],
//     ],
//   });

//   const variables = {
//     network: chain,
//     address: address,
//   };

//   try {
//     return (await graphQLClient.request(
//       gqlQueryFunction,
//       variables
//     )) as unknown;
//   } catch (err) {
//     console.log(err);
//   }
// }
