import {available_chains} from '../common/available-chains.js';
import {CovalentWalletBalanceItem} from '../covalent/covalent-types.js';
import {balances} from '../covalent/covalent-accounts.js';
import Big from 'big.js';
import * as fs from 'fs';
import {getBeefyVaultApy, getBeefyVaultId} from './beefy.js';
import dotenv from 'dotenv';

dotenv.config();

const ignoredTokensPath = process.env.IGNORED_CONTRACTS_JSON as string;
const encoding = 'utf-8';

const ignored_contract_addresses = ((): string[] => {
  if (fs.existsSync(ignoredTokensPath)) {
    const data = fs.readFileSync(ignoredTokensPath, encoding);
    const jsonData = JSON.parse(data).items;

    return jsonData.map(
      (x: {contract_address: unknown}) => x.contract_address as string
    );
  }
  return [];
})();

async function main(): Promise<void> {
  await getBeefyAPYs();
  await getPortfolioTotals();
}

async function getPortfolioTotals() {
  let total = 0.0;
  const chains = available_chains();
  const addresses = (process.env.WALLET_ADDRESSES as string).split(';');
  for (const chain of chains) {
    for (const address of addresses) {
      // note that tokens include the base currency
      const balance = await balances(chain.covalent_chain_id, address);
      for (const token of balance?.items as CovalentWalletBalanceItem[]) {
        const zeroBalance: boolean = Number(token.balance) != 0;
        const ignoreToken = !ignored_contract_addresses.includes(
          token.contract_address
        );
        if (zeroBalance && ignoreToken) {
          const quote_currency = balance?.quote_currency as string;
          const numTokens = Big(token.balance).div(
            Big(10).pow(token.contract_decimals)
          );
          total += token.quote;
          console.log(
            chain.name.padEnd(25) +
              token.contract_ticker_symbol.padEnd(20) +
              token.quote_rate.toString().padEnd(20) +
              numTokens.toString().padEnd(25) +
              token.quote.toFixed(2).padEnd(10) +
              ' ' +
              quote_currency
          );
        }
      }
    }
  }
  console.log('Total value: ' + total.toString());
}

async function getBeefyAPYs(): Promise<void> {
  const beefyTokens = (process.env.BEEFY_TOKENS as string).split(';');
  console.log('Beefy Vault APYs');
  console.log(''.padEnd(50, '-'));
  for (const beefyToken of beefyTokens) {
    const vaultId = await getBeefyVaultId(beefyToken);
    const apy = await getBeefyVaultApy(vaultId);
    console.log(beefyToken.padEnd(20) + apy.padEnd(6) + '%');
  }
  console.log(''.padEnd(50, '-'));
}

void main();
