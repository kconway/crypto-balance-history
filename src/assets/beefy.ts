import axios, {AxiosError, AxiosResponse} from 'axios';
import {axiosErrorHandler} from '../common/axios-error-handler.js';

const throwError = false;
const beefy_vaults_url = 'https://api.beefy.finance/vaults';
const beefy_apy_url = 'https://api.beefy.finance/apy/breakdown';

async function getBeefyVaultId(beefy_symbol: string): Promise<string> {
  try {
    const result = await axios.get(`${beefy_vaults_url}`);
    const beefyVault = result.data.find(
      (x: {earnedToken: string}) => x.earnedToken === beefy_symbol
    );
    return beefyVault?.id;
  } catch (error: AxiosError | unknown) {
    axiosErrorHandler(error, throwError);
    return '';
  }
}

async function getBeefyVaultApy(beefy_vault_id: string): Promise<string> {
  try {
    const result = await axios.get(`${beefy_apy_url}`);
    const beefyVaultApy = Number(result.data[beefy_vault_id]?.totalApy) * 100.0;
    return beefyVaultApy.toFixed(2);
  } catch (error: AxiosError | unknown) {
    axiosErrorHandler(error, throwError);
    return '';
  }
}

export {getBeefyVaultId, getBeefyVaultApy};
