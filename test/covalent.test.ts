import {transactions} from '../src/covalent/covalent-transactions.js';
import assert from 'assert';

describe('Test Covalent API', () => {
  it('covalent-query', async () => {
    const address = 'demo.eth'; // address from covalent website
    const chain_id = '1'; // chain_id: Ethereum Mainnet = 1
    const tStartDate = new Date();
    tStartDate.setMonth(tStartDate.getMonth() - 1);
    const tEndDate = new Date();
    const response = await transactions(
      chain_id,
      address,
      tStartDate,
      tEndDate
    );
    assert.ok(response.length > 0);
  }).timeout(10000);
});
