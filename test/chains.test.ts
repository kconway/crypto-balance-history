import {getChainsFromFile} from '../src//common/available-chains.js';
import assert from 'assert';

describe('Test Chains', () => {
  it('chains-file', () => {
    const response = getChainsFromFile();
    assert.ok(response.length > 0);
  }).timeout(50000);
});
